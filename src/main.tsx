import ReactDOM from "react-dom/client"
import { createBrowserRouter, redirect, RouterProvider } from "react-router-dom"
import HomePage from "./pages/HomePage/HomePage"
import "./index.css"
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage"
import ViewPage from "./pages/ViewPage/ViewPage"
import TestListPage from "./pages/TestListPage/TestListPage"
import AttemptPage from "./pages/AttemptPage/AttemptPage"
import Header from "./components/Header/Header"

const router = createBrowserRouter([
	{
		path: "/",
		element: <HomePage />,
		errorElement: <NotFoundPage />,
		children: [
			{
				path: "/tests/:id",
				loader: async ({ params }) => redirect(`/tests/${params.id}/view`),
			},
			{
				path: "/tests",
				element: <TestListPage />,
			},
			{
				element: <ViewPage />,
				path: "/tests/:id/view",
			},
			{
				element: <AttemptPage />,
				path: "/tests/:id/attempt",
			},
		],
	},
])
ReactDOM.createRoot(document.getElementById("root")!).render(
	<RouterProvider router={router} />
)
