import { Outlet } from "react-router-dom"
import Header from "../../components/Header/Header"
import "./HomePage.scss"
const HomePage = () => {
	return (
		<>
			<Header />
			<div className="container">
				<Outlet />
			</div>
		</>
	)
}
export default HomePage
