import "./styles.scss"
import { Link } from "react-router-dom"
const NotFoundPage = () => {
	return (
		<>
			<div className="error-text">404 Page Not found</div>
			<Link
				style={{
					border: "1px solid black",
					borderRadius: "8px",
					padding: "5px 15px",
				}}
				to="/"
			>
				Back home
			</Link>
			{/* implement going back to previous address instead of homepage */}
		</>
	)
}
export default NotFoundPage
