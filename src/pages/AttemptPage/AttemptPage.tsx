import { Link, Navigate, useParams } from "react-router-dom"
import Test from "../../components/Test/Test"
function AttemptPage() {
	const correctAnswers: { [key: string]: string[] } = {
		biden: [`of coursen't`],
		sf: [`shadow fiend`, `nevermore`, "yaphets", `shadowrazer`],
	}
	const { id } = useParams()
	const attemptToken = id ? localStorage.getItem(id) : undefined
	const questions = [
		{
			id: "biden",
			questionText: "would you hit biden in shoulder for five gummy bears?",
			values: [`yes`, `yesn't`, "of course", `of coursen't`],
			multichoice: false,
		},
		{
			id: "sf",
			questionText: "arcwarden or shadow fiend?",
			values: [`shadow fiend`, `nevermore`, "yaphets", `shadowrazer`],
			multichoice: true,
		},
	]
	if (!attemptToken) return <Navigate to={`/tests/${id}/view`} />

	return <Test questions={questions} correctAnswers={correctAnswers} />
}

export default AttemptPage
