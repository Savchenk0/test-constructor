import { Link } from "react-router-dom"

type AssignmentDataT = {
	name: string
	id: string
}

const TestListPage = () => {
	const assignmentsData: AssignmentDataT[] = [
		{ name: "test1", id: "test1_id" },
		{ name: "test2", id: "test2_id" },
	]
	// TODO: add real data, probably from global state manager
	return (
		<>
			<h1>Your tests</h1>
			<div
				style={{
					display: "flex",
					width: "800px",
					flexWrap: "wrap",
					gap: "30px",
				}}
			>
				{assignmentsData.map(({ id, name }) => (
					<p key={id}>
						<Link to={`/tests/${id}`}>{name}</Link>
					</p>
				))}
			</div>
		</>
	)
}
export default TestListPage
