import { Link, useParams } from "react-router-dom"
import AttemptSummary from "../../components/AttemptSummary/AttemptSummary"

interface AttemptRecordT {
	score: number
	date: Date
	duration: number
}
interface AttemptsT {
	left: number
	record: AttemptRecordT[]
}
const ViewPage = () => {
	const { id } = useParams()
	const testName = id
	//TODO: Делаем по айдишнику запрос на инфу необходимую для корректного отображения вью пейдж и добавляет testName
	const attempts: AttemptsT = {
		left: 1,
		record: [],
	}
	// TODO: добавить получение количества попыток и информации о уже сделаных извне
	// TODO: сами данные о ппоытке нам здесь не нужны, надо разделить score от details, я не хочу подгружать details на ViewPage поскольку я здесь никак не буду ими пользоваться
	const { left, record } = attempts
	return (
		<>
			<h1>This is a preview page for test {testName}</h1>
			{record.length ? <AttemptSummary /> : null}
			{/* TODO:добавить компонент AttemptSummary который будет однострочным представлением результата пройденного теста(см пример в мудле)  */}
			{left ? (
				<Link
					style={{
						border: "1px solid black",
						borderRadius: "8px",
						padding: "5px 15px",
					}}
					onClick={() => {
						if (id) localStorage.setItem(id, "attempt")
					}}
					to={`/tests/${id}/attempt`}
				>
					Start Test
				</Link>
			) : null}
		</>
	)
}
export default ViewPage
