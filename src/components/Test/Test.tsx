import { Button, Form, Modal } from "antd"
import Loader from "../Loader/Loader"
import type { FormProps } from "antd"
import { CheckboxValueType } from "antd/es/checkbox/Group"
import Question from "../Question/Question"
import "./styles.scss"
import { useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
export interface AnswersT {
	[key: string]: string[]
}
export interface QuestionT {
	id: string
	questionText: string
	values: string[]
	multichoice: boolean
}
interface TestT {
	questions: QuestionT[]
	correctAnswers: { [key: string]: string[] }
}
const countPoints = (answers: AnswersT, correctAnswers: AnswersT) => {
	// TODO: сделать более гибкий подсчет баллов для мультичойс вопросов
	const questions = Object.keys(correctAnswers)
	const result = questions.reduce((accumulator, currentValue) => {
		if (
			correctAnswers[currentValue]?.length !== answers[currentValue]?.length
		) {
			return accumulator
		}
		for (let i = 0; i < correctAnswers[currentValue]?.length; i++) {
			if (!answers[currentValue].includes(correctAnswers[currentValue][i])) {
				return accumulator
			}
		}
		return accumulator + 1
	}, 0)
	return result
}
const Test = ({ questions, correctAnswers }: TestT) => {
	const [form] = Form.useForm()
	const [openModal, setOpenModal] = useState(false)
	const [isAllDone, setIsAllDone] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const navigate = useNavigate()
	const { id } = useParams()
	const modalText = isAllDone
		? "After submitting you won't be able to change answers. Want to edit something?"
		: "Seems like you still got unanswered questions, might want to go back and fill them"

	const showModal = () => {
		setOpenModal(true)
	}
	const handleSubmit = () => {
		setIsLoading(true)
		try {
			const answers = form.getFieldsValue()
			const score = countPoints(answers, correctAnswers)
			const attemptDetails = { score, answers }
			console.log("attempt details: ", attemptDetails)
			// TODO: добавить пут запрос с attemptDetails туда где будут храниться подобные данные
		} catch (error) {
			if (error instanceof Error) {
				alert(error.message)
			} else {
				alert(String(error))
			}
			return
		}
		setTimeout(() => {
			setIsLoading(false)
			navigate(`/tests/${id}/view`)
		}, 500)
	}

	const handleCancel = () => {
		setOpenModal(false)
	}
	const onFinish: FormProps<AnswersT>["onFinish"] = answers => {
		if (Object.keys(answers).find(e => answers[e] === undefined) === undefined)
			setIsAllDone(true)
		showModal()
		//TODO: когда добавлю более гибкое распределение очков за вопросы (за какой-то можно получить больше за какой-то меньше баллов) позаботиться о подсчете тотал поинтс(возможно добавить саму структуру данных questions отдельным полем, возможно считать динамически здесь, возможно где-то ещё(2й пункт меньше всего привлекает))
	}
	const onFinishFailed: FormProps<AnswersT>["onFinishFailed"] = errorInfo => {
		console.log("Failed:", errorInfo)
	}
	const onValueChange = (name: string, value: string | CheckboxValueType[]) => {
		form.setFieldsValue({ [name]: value })
	}

	return (
		<Form
			form={form}
			className="form-wrapper"
			name="basic"
			onFinish={onFinish}
			onFinishFailed={onFinishFailed}
			autoComplete="off"
		>
			{questions.map(({ id, ...rest }) => (
				<Form.Item key={id} name={id}>
					<Question id={id} selectionChange={onValueChange} {...rest} />
				</Form.Item>
			))}

			<Button className="modal-openBtn" type="primary" htmlType="submit">
				Finish test
			</Button>
			<Modal
				open={openModal}
				onCancel={handleCancel}
				centered
				footer={() => (
					<div className="modal-footer">
						<Button
							style={{ padding: "4px 15px" }}
							key="back"
							onClick={handleCancel}
						>
							Edit answers
						</Button>
						<Button key="submit" type="primary" onClick={handleSubmit}>
							Answers are correct, submit!
						</Button>
					</div>
				)}
			>
				{!isLoading && <h2 className="modal-text">{modalText}</h2>}
				<Loader isLoading={isLoading} />
			</Modal>
		</Form>
	)
}
export default Test
