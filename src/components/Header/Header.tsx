import { Link } from "react-router-dom"
import "./styles.scss"
const Header = () => {
	return (
		<header>
			<div className="header-background">
				<div className="header-content">
					<img src="/logo-64.png" />
					<ul className="header-content__list">
						<li className="header-content__list-item">
							<Link to="/">Home</Link>
						</li>
						<li className="header-content__list-item">
							<Link to="/tests">Tests</Link>
						</li>
						<li className="header-content__list-item">
							<Link to="/constructor">Constructor</Link>
						</li>
					</ul>
				</div>
			</div>
		</header>
	)
}
export default Header
