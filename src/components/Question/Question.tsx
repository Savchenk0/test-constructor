import { Checkbox, Radio } from "antd"
import { CheckboxValueType } from "antd/es/checkbox/Group"
import "./styles.scss"
type QuestionPropsT = {
	questionText: string
	values: string[]
	multichoice: boolean
	id: string
	selectionChange: (name: string, value: CheckboxValueType[]) => void
}
function Question({
	questionText,
	values,
	multichoice,
	selectionChange,
	id,
}: QuestionPropsT) {
	return (
		<>
			<h2 className="test-question">{questionText}</h2>
			{multichoice ? (
				<Checkbox.Group
					style={{
						display: "flex",
						flexDirection: "column",
					}}
					options={values}
					onChange={values => selectionChange(id, values)}
				/>
			) : (
				<Radio.Group
					style={{
						display: "flex",
						flexDirection: "column",
					}}
					onChange={e => {
						selectionChange(id, [e.target.value])
					}}
				>
					{values.map(option => (
						<Radio style={{ border: "3px" }} key={option} value={option}>
							{option}
						</Radio>
					))}
				</Radio.Group>
			)}
		</>
	)
}
export default Question
