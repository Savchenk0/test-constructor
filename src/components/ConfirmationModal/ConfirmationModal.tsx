import { useState } from "react"
import { Button, Modal } from "antd"
import "./styles.scss"
import { Form } from "antd"
type ConfirmationModalT = {
	buttonText: string
}
const ConfirmationModal = ({ buttonText }: ConfirmationModalT) => {
	const [loading, setLoading] = useState(false)
	const [open, setOpen] = useState(false)

	const showModal = () => {
		setOpen(true)
	}

	const handleOk = () => {
		setLoading(true)
		setTimeout(() => {
			setLoading(false)
			setOpen(false)
		}, 1000)
	}

	const handleCancel = () => {
		setOpen(false)
	}

	return (
		<>
			<Button className="modal-openBtn" type="primary" onClick={showModal}>
				{buttonText}
			</Button>
			<Modal
				open={open}
				centered
				// title="Title"
				onOk={handleOk}
				onCancel={handleCancel}
				footer={() => (
					<div className="modal-footer">
						<Button key="back" onClick={handleCancel}>
							Edit answers
						</Button>
						<Button
							key="submit"
							type="primary"
							htmlType="submit"
							loading={loading}
							onClick={handleOk}
						>
							Answers are correct, submit!
						</Button>
					</div>
				)}
			>
				<h2 className="modal-text">Are you sure you're all set?</h2>
			</Modal>
		</>
	)
}

export default ConfirmationModal
