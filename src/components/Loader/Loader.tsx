import { LoadingOutlined } from "@ant-design/icons"
import { Spin } from "antd"

interface LoaderI {
	isLoading: boolean
}
const Loader = ({ isLoading }: LoaderI) => {
	if (!isLoading) return null
	return (
		<div
			className="LoaderWrapper"
			style={{
				display: "flex",
				justifyContent: "center",
				padding: "25px 0",
			}}
		>
			<Spin indicator={<LoadingOutlined style={{ fontSize: 44 }} spin />} />
		</div>
	)
}
export default Loader
